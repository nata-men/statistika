package com.Prognoz.Repo;

import com.Prognoz.Statis;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StatisRepo extends JpaRepository<Statis,Integer> {

}
