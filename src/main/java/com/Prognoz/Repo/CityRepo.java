package com.Prognoz.Repo;

import com.Prognoz.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CityRepo extends JpaRepository<City,Integer> {
    @Query (value = "SELECT * FROM city where fuel=:fuel",nativeQuery = true)
     List<City> findAllByfuel(@Param("fuel") String fuel);

}
