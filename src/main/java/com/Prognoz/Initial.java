package com.Prognoz;

import com.Prognoz.Repo.StatisRepo;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class Initial {
    public static void  trend (List<Statis> dates, int lenSeason){
        double alfa = 0.2;
        double beta = 0.2;
        double gama = 0.45;
        double sum = 0;
        double k;
        for (int i = 0; i < lenSeason; i++){
            k = (dates.get(i+lenSeason).getValue_varety() - dates.get(i).getValue_varety())/lenSeason;
            sum = sum + k;
        }
        dates.get(0).setTrend(sum / lenSeason);

        dates.get(0).setSmooth(dates.get(0).getValue_varety());
        for (int i = 1; i < lenSeason; i++){
            dates.get(i).setSmooth(alfa*dates.get(i).getValue_varety()+(1-alfa)*(dates.get(i-1).getSmooth()-dates.get(i-1).getTrend()));
            dates.get(i).setTrend(beta*(dates.get(i).getSmooth()-dates.get(i-1).getSmooth())+(1-beta)*dates.get(i-1).getTrend());
        }
    }

   public static void season (List<Statis> dates, int lenSeason){
        int n_season = dates.size()/lenSeason;
        double sum = 0;
        double[] qseasons = new double[n_season];
        for (int i = 0; i < n_season; i++){
            for (int j = 0; j < lenSeason;j++){
                sum = sum + dates.get(j+i*lenSeason).getValue_varety();
            }
            qseasons[i] = sum/lenSeason;
            sum = 0;
        }
        for (int i = 0; i < lenSeason; i++){
            for (int j=0; j < n_season; j++){
                sum = sum + dates.get(i+j*lenSeason).getValue_varety()/qseasons[j];
            }
            dates.get(i).setSeason(sum/n_season);
            sum = 0;
        }
    }

     public static void predprognoz (List<Statis> dates, int lenSeason){
        double alfa = 0.2;
        double beta = 0.2;
        double gama = 0.45;
        for (int i = lenSeason; i < 2*lenSeason; i++){
            dates.get(i).setSmooth(alfa*dates.get(i).getValue_varety()/dates.get(i-lenSeason).getSeason()+(1-alfa)*(dates.get(i-1).getSmooth()-dates.get(i-1).getTrend()));
            dates.get(i).setTrend(beta*(dates.get(i).getSmooth()-dates.get(i-1).getSmooth())+(1-beta)*dates.get(i-1).getTrend());
            dates.get(i).setSeason(gama*dates.get(i).getValue_varety()/dates.get(i).getSmooth()+(1-gama)*dates.get(i-lenSeason).getSeason());
        }
    }



    public static void prognoz (List<Statis> dates, int lenSeason){
        double v;
        for (int i = 2*lenSeason; i < 3*lenSeason;i++){
            v = (dates.get(i - 12).getSmooth() + (12 * dates.get(i - 12).getTrend())) * dates.get(i - lenSeason).getSeason();
            dates.get(i).setValue_prognoz(v);
            dates.add(new Statis( 0, Times.addYaer(dates.get(i-12).getDate(),1),0,0,0,v));
        }
    }
}
