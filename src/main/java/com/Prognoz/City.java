package com.Prognoz;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name ="city")
public class City{
        @Id
        @GeneratedValue(strategy = GenerationType.TABLE)
        private int id;
        @Column(name = "name_city")
        private String name_city;
        @Column(name = "value_stat")
        private double value_stat;
        @Column(name = "lan")
        private double lan;
        @Column(name = "lng")
        private double lng;
        @Column (name = "fuel")
        private String fuel;

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }


    public City() {

    }

    public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName_city() {
            return name_city;
        }

        public void setName_city(String name_city) {
            this.name_city = name_city;
        }

        public double getValue_stat() {
            return value_stat;
        }

        public void setValue_stat(double value_stat) {
            this.value_stat = value_stat;
        }

        public double getLan() {
            return lan;
        }

        public void setLan(double lan) {
            this.lan = lan;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }

}