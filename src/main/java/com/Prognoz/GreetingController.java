package com.Prognoz;

import com.Prognoz.Repo.CityRepo;
import com.Prognoz.Repo.StatisRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;

import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.util.Date;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
@EnableScheduling
@Controller
public class GreetingController {

    @Autowired
    private StatisRepo statisRepo;
    @Autowired
    private CityRepo cityRepo;


    @GetMapping("/greeting")
    public String greeting(Map <String, Object> model) {
        //Initial.trend( statisRepo.findAll(),12);
        //Initial.season(statisRepo.findAll(),12);
        //Initial.predprognoz(statisRepo.findAll(),12);

       // Initial.prognoz(statisRepo.findAll(),12);

       // statisRepo.saveAll(statisRepo.findAll());

        Iterable<Statis> statis =null;
        model.put("ststistica", statis);
        return "greeting";
    }

    @GetMapping("/maps")
    public String maps(Map <String, Object> model){
        Iterable<City> cities = null;
        model.put("cd", cities);
        return "maps";
    }

    @PostMapping
    public String add(@RequestParam String fuel,Map <String, Object> model){
        Iterable<City> cities = cityRepo.findAllByfuel(fuel);
        model.put("cd", cities);
        return "maps";
    }
}
