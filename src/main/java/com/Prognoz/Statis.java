package com.Prognoz;


import org.springframework.boot.autoconfigure.web.ResourceProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table (name ="sy")
public class Statis {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int id;
    @Column (name = "value_varety")
    private double value_varety;
    @Column (name = "date")
    private Date date;
    @Column (name = "trend")
    private double trend;
    @Column (name = "smooth")
    private double smooth;
    @Column (name = "season")
    private double season;
    @Column (name = "value_prognoz")
    private double value_prognoz;


    public Statis(double value_varety, Date date, double trend, double smooth, double season, double value_prognoz) {
        this.value_varety = value_varety;
        this.date = date;
        this.trend = trend;
        this.smooth = smooth;
        this.season = season;
        this.value_prognoz = value_prognoz;
    }



    public Statis() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValue_varety() {
        return value_varety;
    }

    public Date getDate() {
        return date;
    }

    public double getTrend() {
        return trend;
    }

    public double getSmooth() {
        return smooth;
    }

    public double getSeason() {
        return season;
    }

    public double getValue_prognoz() {
        return value_prognoz;
    }

    public void setValue_varety(double value_varety) {
        this.value_varety = value_varety;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTrend(double trend) {
        this.trend = trend;
    }

    public void setSmooth(double smooth) {
        this.smooth = smooth;
    }

    public void setSeason(double season) {
        this.season = season;
    }

    public void setValue_prognoz(double value_prognoz) {
        this.value_prognoz = value_prognoz;
    }

}

